# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Line                                             | 1~5%     | Y         |
| Horizontal flip                                  | 1~5%     | Y         |
| Vertical flip                                    | 1~5%     | Y         |
| Counterclockwise rotation                        | 1~5%     | Y         |
| Clockwise rotation                               | 1~5%     | Y         |


---

### How to use 

![image](report/color.PNG) : Color. Click down and select the color of brush/text/shape.

![image](report/brush_size.PNG) : Brush size. Drag the gray point and change the size of brush/eraser/the border of shape. The min value is 1, and the max value is 30.

![image](report/text_font.PNG) : Text font. Click and select the font you want.

![image](report/text_size.PNG) : Text size. Click and select the size you want.

![image](report/fill.PNG) : Check the box and the next shape (circle/triangle/rectangle) you draw will be filled.

![image](icon/pencil_s.png) : Brush/pencil, mousedown at the canvas, and it will draw as your mouse moves.

![image](icon/eraser_s.png) : Eraser, mousedown at the canvas, and it will erase the path that your mouse moves.

![image](icon/text_editor_s.png) : Text editor, mousedown at the canvas, type on the input box, keydown at "Enter", and the text will be created on the canvas.

![image](icon/circle_s.png) : Circle, mousedown at the canvas, and drag your mouse to the position that you want.

![image](icon/triangle_s.png) : Triangle, mousedown at the canvas, and drag your mouse to the position that you want.

![image](icon/rectangle_s.png) : Rectangle, mousedown at the canvas, and drag your mouse to the position that you want.

![image](icon/upload_s.png) : Upload, click and select the image you want to upload. The image will be resized if its width or height is over 600 px.

![image](icon/download_s.png) : Download, click and the existing canvas will be download as PNG to your computer.

![image](icon/undo_s.png) : Undo, click and the existing canvas will be restored to the previous state.

![image](icon/redo_s.png) : Redo, click and the existing canvas will be restored to the next state.

![image](icon/refresh_s.png) : Refresh, click and the existing canvas will be cleared.


### Function description


![image](icon/line_s.png) : Line, mousedown at the canvas, and drag your mouse to the position that you want.

![image](icon/horizontal_s.png) : Horizontal flip, click and the existing canvas will be fliped horizontally.

![image](icon/vertical_s.png) : Vertical flip, click and the existing canvas will be fliped vertically.

![image](icon/counterclockwise_s.png) : Counterclockwise rotation, click and the existing canvas will be rotated 90 degrees counterclockwise.

![image](icon/clockwise_s.png) : Clockwise rotation, click and the existing canvas will be rotated 90 degrees clockwise.


### Gitlab page link

[https://108062111.gitlab.io/AS_01_WebCanvas/](https://108062111.gitlab.io/AS_01_WebCanvas/)


### Others (Optional)

    Thank you TAs!
