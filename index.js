var status = "";
var isDrawing = false;
var isTyping = false;
var color = "black";
var size = 5;
var filled = false;
var text_font = "Arial";
var text_size = "12";
var history_states = [];
var history_idx = 0;

// on computer or on mobile
const hasTouchEvent = 'ontouchstart' in window ? true : false;
const downEvent = hasTouchEvent ? 'ontouchstart' : 'mousedown';
const moveEvent = hasTouchEvent ? 'ontouchmove' : 'mousemove';
const upEvent = hasTouchEvent ? 'touchend' : 'mouseup';

window.onload = function() {
    initApp();
}

function initApp() {
    updateStatus("pencil");

    document.getElementById('color_selector').addEventListener('change', onColorSelector);
    document.getElementById('brush_size').addEventListener('change', onBrushsize);

    document.getElementById('text_font').addEventListener('change', onTextFont);
    document.getElementById('text_size').addEventListener('change', onTextSize);
    
    document.getElementById('filled').addEventListener('change', onFilled);
    
    document.getElementById('pencil').addEventListener('click', onPencil);
    document.getElementById('eraser').addEventListener('click', onEraser);
    document.getElementById('text_editor').addEventListener('click', onTextEditor);
    
    document.getElementById('circle').addEventListener('click', onCircle);
    document.getElementById('triangle').addEventListener('click', onTriangle);
    document.getElementById('rectangle').addEventListener('click', onRectangle);
    document.getElementById('line').addEventListener('click', onLine);

    document.getElementById('myImg').addEventListener('click', checkUpload);
    document.getElementById('myImg').addEventListener('change', onUpload);
    document.getElementById('download').addEventListener('click', onDownload);

    document.getElementById('counterclockwise').addEventListener('click', onCounterclockwise);
    document.getElementById('clockwise').addEventListener('click', onClockwise);
    document.getElementById('horizontal').addEventListener('click', onHorizontal);
    document.getElementById('vertical').addEventListener('click', onVertical);

    document.getElementById('undo').addEventListener('click', onUndo);
    document.getElementById('redo').addEventListener('click', onRedo);
    document.getElementById('refresh').addEventListener('click', checkRefresh);
    

    var x = 0, y = 0, og_x = 0, og_y = 0;

    const myCanvas = document.getElementById('myCanvas');
    const ctx = myCanvas.getContext('2d');
    const tmpCanvas = document.getElementById('tmpCanvas');
    const tmpCtx = tmpCanvas.getContext('2d');
    history_states.push(ctx.getImageData(0, 0, myCanvas.width, myCanvas.height));

    // event.offsetX, event.offsetY gives the (x,y) offset from the edge of the canvas.

    // Add the event listeners for mousedown, mousemove, and mouseup
    myCanvas.addEventListener(downEvent, e => {
        ctx.lineCap = 'round' // 線條兩端圓弧
        ctx.lineJoin = 'round' // 線條折角圓弧
        ctx.strokeStyle = color;
        ctx.fillStyle = color;
        ctx.lineWidth = size;
        ctx.font = text_size + 'px ' + text_font;
        x = e.offsetX;
        y = e.offsetY;
        if(status == "pencil" || status == "eraser" || status == "text_editor" ||
           status == "circle" || status == "triangle" || status == "rectangle" ||
           status == "line"){
            isDrawing = true;
        }
        if(status == "circle" || status == "triangle" || status == "rectangle" ||
           status == "line"){
            og_x = x;
            og_y = y;
            tmpCtx.clearRect(0, 0, tmpCanvas.width, tmpCanvas.height);
            tmpCtx.drawImage(myCanvas, 0, 0);
            ctx.clearRect(0, 0, myCanvas.width, myCanvas.height);
        }
    });

    myCanvas.addEventListener(moveEvent, e => {
        if(isDrawing){
            if (status == "pencil") {
                drawLine(ctx, x, y, e.offsetX, e.offsetY);
            }
            else if(status == "eraser"){
                eraseLine(ctx, x, y, e.offsetX, e.offsetY);
            }
            else if(status == "circle"){
                drawCir(myCanvas, ctx, og_x, og_y, e.offsetX, e.offsetY);
            }
            else if(status == "triangle"){
                drawTri(myCanvas, ctx, og_x, og_y, e.offsetX, e.offsetY);
            }
            else if(status == "rectangle"){
                drawRect(myCanvas, ctx, og_x, og_y, e.offsetX, e.offsetY);
            }
            else if(status == "line"){
                ctx.clearRect(0, 0, myCanvas.width, myCanvas.height);
                drawLine(ctx, og_x, og_y, e.offsetX, e.offsetY);
            }
        }
        x = e.offsetX;
        y = e.offsetY;
    });

    window.addEventListener(upEvent, e => {
        if(isTyping){
            isTyping = false;
            document.body.removeChild(document.getElementById('tmp'));
        }
        if(isDrawing){
            if (status == "pencil") {
                drawLine(ctx, x, y, e.offsetX, e.offsetY);
            }
            else if(status == "eraser"){
                eraseLine(ctx, x, y, e.offsetX, e.offsetY);
            }
            else if(status == "text_editor"){
                createText(myCanvas, ctx, e.offsetX, e.offsetY);
            }
            else if(status == "circle" || status == "triangle" || status == "rectangle" ||
                    status == "line"){
                tmpCtx.drawImage(myCanvas, 0, 0);
                ctx.drawImage(tmpCanvas, 0, 0);
                tmpCtx.clearRect(0, 0, tmpCanvas.width, tmpCanvas.height);
            }

            if(status != "text_editor"){
                saveState(myCanvas, ctx);
            }
        }
        isDrawing = false;
        x = e.offsetX;
        y = e.offsetY;
    });

    
}

function saveState(myCanvas, ctx) {
    if(history_idx != history_states.length - 1){
        history_states = history_states.slice(0, history_idx + 1);
    }
    history_states.push(ctx.getImageData(0, 0, myCanvas.width, myCanvas.height));
    history_idx++;
}

function drawLine(ctx, x1, y1, x2, y2) {
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
    ctx.closePath();
}

function eraseLine(ctx, x1, y1, x2, y2) {
    ctx.globalCompositeOperation = 'destination-out';
    drawLine(ctx, x1, y1, x2, y2)
    ctx.globalCompositeOperation = 'source-over';
}

function createText(myCanvas, ctx, x2, y2) {
    var input = document.createElement('input');
    input.type = 'text';
    input.id = 'tmp';
    input.style.position = 'fixed';
    input.style.left = (x2 + 480) + 'px';
    input.style.top = y2 + 'px';
    input.style.zIndex = '999';
    input.onkeydown = e => {
        if(e.key == "Enter") {
            console.log("font: ", ctx.font);
            ctx.fillText(input.value, x2, y2);
            saveState(myCanvas, ctx);
            document.body.removeChild(input);
            isTyping = false;
        }
    }
    // document.getElementById('right').appendChild(input);
    document.body.appendChild(input);
    input.focus();
    isTyping = true;
}

function drawCir(myCanvas, ctx, x1, y1, x2, y2) {
    ctx.clearRect(0, 0, myCanvas.width, myCanvas.height);
    ctx.beginPath();
    var x_dif = x1 - x2;
    var x_sum = x1 + x2;
    var y_dif = y1 - y2;
    var y_sum = y1 + y2;
    ctx.arc(x_sum / 2, y_sum / 2, Math.hypot(x_dif, y_dif) / 2, 0, 2 * Math.PI);
    if(filled) ctx.fill();
    else ctx.stroke();
    ctx.closePath();
}

function drawTri(myCanvas, ctx, x1, y1, x2, y2) {
    ctx.clearRect(0, 0, myCanvas.width, myCanvas.height);
    ctx.beginPath();
    var x_sum = x1 + x2;
    ctx.stroke();
    ctx.moveTo(x_sum / 2, y1);
    ctx.lineTo(x1, y2);
    ctx.lineTo(x2, y2);
    ctx.closePath();
    if(filled) ctx.fill();
    else ctx.stroke();
}

function drawRect(myCanvas, ctx, x1, y1, x2, y2) {
    ctx.clearRect(0, 0, myCanvas.width, myCanvas.height);
    ctx.beginPath();
    if(filled) ctx.fillRect(x1, y1, x2-x1, y2-y1);
    else ctx.strokeRect(x1, y1, x2-x1, y2-y1);
    ctx.closePath();
}

function updateStatus(newStatus) {
    console.log(newStatus, " was clicked.");
    if(status != ""){
        document.getElementById(status).style.boxShadow = "";
    }
    status = newStatus;
    document.getElementById(status).style.boxShadow = "0 0 5px black";
    var cursor_src = 'url("icon/' + newStatus + '_s.png") 0 32, auto';
    document.body.style.cursor = cursor_src;
}

function onFilled() {
    filled = this.checked;
    console.log("filled: ", this.checked);
}

function onBrushsize() {
    size = this.value;
    console.log("brush_size: ", this.value);
}

function onTextFont() {
    text_font = this.value;
    console.log("text_font: ", text_font);
}

function onTextSize() {
    text_size = this.value;
    console.log("text_size: ", text_size);
}

function onColorSelector() {
    color = this.value;
    console.log("color: ", this.value);
}

function onPencil() {
    updateStatus("pencil");
}

function onEraser() {
    updateStatus("eraser");
}

function onTextEditor() {
    updateStatus("text_editor");
}

function onCircle() {
    updateStatus("circle");
}

function onTriangle() {
    updateStatus("triangle");
}

function onRectangle() {
    updateStatus("rectangle");
}

function onLine() {
    updateStatus("line");
}

function checkUpload() {
    alert("Make sure you have saved the canvas before you continue to upload the new image.");
}

function onUpload() {
    updateStatus("upload");
    const myCanvas = document.getElementById('myCanvas');
    const tmpCanvas = document.getElementById('tmpCanvas');
    const ctx = myCanvas.getContext('2d');
    var newImg = new Image();
    newImg.onload = function(){
        var w = (this.width < 600) ? this.width : 600;
        var h = (this.width < 600) ? this.height : this.height * 600 / this.width;
        if(h > 600){
            w = w * 600 / h;
            h = 600;
        }
        myCanvas.width = w;
        myCanvas.height = h;
        tmpCanvas.width = w;
        tmpCanvas.height = h;
        ctx.drawImage(newImg, 0, 0, w, h);
        saveState(myCanvas, ctx);
    };
    newImg.src = URL.createObjectURL(this.files[0]);
}

function onDownload() {
    updateStatus("download");
    const myCanvas = document.getElementById('myCanvas');
    const dl = document.getElementById('download');
    dl.href = myCanvas.toDataURL();
}

function myRotate(v_skew, h_skew, dx, dy) {
    const myCanvas = document.getElementById('myCanvas');
    const ctx = myCanvas.getContext('2d');
    const tmpCanvas = document.getElementById('tmpCanvas');
    const tmpCtx = tmpCanvas.getContext('2d');
    const w = myCanvas.width;
    const h = myCanvas.height;
    tmpCtx.drawImage(myCanvas, 0, 0);
    ctx.clearRect(0, 0, w, h);

    myCanvas.width = h;
    myCanvas.height = w;

    ctx.setTransform(0, v_skew, h_skew, 0, dx, dy);
    ctx.drawImage(tmpCanvas, 0, 0);
    // reset ctx to the default
    ctx.setTransform(1, 0, 0, 1, 0, 0);

    tmpCtx.clearRect(0, 0, w, h);
    tmpCanvas.width = h;
    tmpCanvas.height = w;
    saveState(myCanvas, ctx);
}

function onCounterclockwise() {
    updateStatus("counterclockwise");
    const myCanvas = document.getElementById('myCanvas');
    myRotate(-1, 1, 0, myCanvas.width);
}

function onClockwise() {
    updateStatus("clockwise");
    const myCanvas = document.getElementById('myCanvas');
    myRotate(1, -1, myCanvas.height, 0);
}

function myFlip(x_scale, y_scale, x_og, y_og) {
    const myCanvas = document.getElementById('myCanvas');
    const ctx = myCanvas.getContext('2d');
    const tmpCanvas = document.getElementById('tmpCanvas');
    const tmpCtx = tmpCanvas.getContext('2d');
    tmpCtx.drawImage(myCanvas, 0, 0);
    ctx.clearRect(0, 0, myCanvas.width, myCanvas.height);
    ctx.save();
    ctx.scale(x_scale, y_scale);
    ctx.beginPath();
    ctx.drawImage(tmpCanvas, x_og, y_og);
    ctx.restore();
    tmpCtx.clearRect(0, 0, myCanvas.width, myCanvas.height);
    saveState(myCanvas, ctx);
}

function onHorizontal() {
    updateStatus("horizontal");
    const myCanvas = document.getElementById('myCanvas');
    // Multiply the x value by -1 to flip horizontally
    // Start at (-width, 0), which is now the top-right corner
    myFlip(-1, 1, -myCanvas.width, 0);
}

function onVertical() {
    updateStatus("vertical");
    const myCanvas = document.getElementById('myCanvas');
    // Multiply the y value by -1 to flip vertically
    // Start at (0, -height), which is now the bottom-left corner
    myFlip(1, -1, 0, -myCanvas.height);
}

function onUndo() {
    updateStatus("undo");
    const myCanvas = document.getElementById('myCanvas');
    const ctx = myCanvas.getContext('2d');
    const tmpCanvas = document.getElementById('tmpCanvas');
    if(history_idx > 0){
        myCanvas.width = history_states[history_idx-1].width;
        myCanvas.height = history_states[history_idx-1].height;
        tmpCanvas.width = history_states[history_idx-1].width;
        tmpCanvas.height = history_states[history_idx-1].height;
        ctx.putImageData(history_states[--history_idx], 0, 0);
    }
}

function onRedo() {
    updateStatus("redo");
    const myCanvas = document.getElementById('myCanvas');
    const ctx = myCanvas.getContext('2d');
    const tmpCanvas = document.getElementById('tmpCanvas');
    if(history_idx < history_states.length - 1){
        myCanvas.width = history_states[history_idx+1].width;
        myCanvas.height = history_states[history_idx+1].height;
        tmpCanvas.width = history_states[history_idx+1].width;
        tmpCanvas.height = history_states[history_idx+1].height;
        ctx.putImageData(history_states[++history_idx], 0, 0);
    }
}

function checkRefresh() {
    var r = confirm("Confirm to delete the existing drawing.");
    if (r == true) {
        onRefresh();
    }
}

function onRefresh() {
    updateStatus("refresh");
    const myCanvas = document.getElementById('myCanvas');
    const ctx = myCanvas.getContext('2d');
    const tmpCanvas = document.getElementById('tmpCanvas');
    const tmpCtx = tmpCanvas.getContext('2d');
    clear(myCanvas, ctx);
    clear(tmpCanvas, tmpCtx);
    saveState(myCanvas, ctx);
}

function clear(myCanvas, ctx) {
    ctx.clearRect(0, 0, myCanvas.width, myCanvas.height);
    myCanvas.width = 600;
    myCanvas.height = 600;
}

/*
reference:
https://developer.mozilla.org/zh-TW/docs/Web/API/Canvas_API
https://developer.mozilla.org/en-US/docs/Web/API/Element/mousedown_event
http://skyroxas.tw/canvas-%E4%BD%BF%E7%94%A8-javascript-%E5%AF%A6%E4%BD%9C-canvas-%E7%95%AB%E7%AD%86%E7%9A%84%E5%8A%9F%E8%83%BD/
https://stackoverflow.com/questions/21011931/how-to-embed-an-input-or-textarea-in-a-canvas-element
*/